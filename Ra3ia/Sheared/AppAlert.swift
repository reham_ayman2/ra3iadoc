//
//  AppAlert.swift
//  SeaTrips
//
//  Created by Sara Mady on 11/4/20.
//  Copyright © 2020 Sara Ashraf. All rights reserved.
//

import Foundation
import StatusAlert
import UIKit


    func showSuccessAlert(title:String,message:String){
        let statusAlert = StatusAlert()
        statusAlert.image = #imageLiteral(resourceName: "check")
        statusAlert.title = title
        statusAlert.message = message
        statusAlert.canBePickedOrDismissed = true

        // Presenting created instance
        statusAlert.showInKeyWindow()
    }
    
    func showErrorAlert(title:String,message:String){
        let statusAlert = StatusAlert()
       
        statusAlert.image = #imageLiteral(resourceName: "close")
        statusAlert.title = title
        statusAlert.message = message
        statusAlert.canBePickedOrDismissed = true

        // Presenting created instance
        statusAlert.showInKeyWindow()
    }

func showNoInterNetAlert(){
    let statusAlert = StatusAlert()
   
    statusAlert.image = #imageLiteral(resourceName: "cloud")
    statusAlert.title = ""
    statusAlert.message =  "Make sure you are connected to the Internet!" .localized
    statusAlert.canBePickedOrDismissed = true

    // Presenting created instance
    statusAlert.showInKeyWindow()
}

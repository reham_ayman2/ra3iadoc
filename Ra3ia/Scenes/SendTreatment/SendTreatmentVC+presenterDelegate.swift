//
//  SendTreatmentVC+presenterDelegate.swift
//  Ra3ia
//
//  Created by Reham Ayman on 3/24/21.
//

import UIKit
extension SendTreatmentVC : SendTreatmentView {
    func fetchingDataSuccess() {
        self.tableview.reloadData()
    }
    
    func SetUpTableview() {
        tableview.delegate = self
        tableview.dataSource = self
        tableview.separatorStyle = .none
    }
    
    func HandelAlertSheet () {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)

        actionSheet.addAction(UIAlertAction(title: "cats", style: .default, handler: { (Action) in
            
            print("cats selected")
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Dogs", style: .default, handler: { (action) in
            
            print("dogs selected ")
        }))
        
        actionSheet.addAction(UIAlertAction(title: "birds ", style: .default, handler: { (action) in
            print ("birds selected ")
        }))
        
        actionSheet.addAction(UIAlertAction(title: "rats ", style: .default, handler: { (action) in
            print ("birds selected ")
        }))
                
      
        actionSheet.view.tintColor = UIColor(named: "BaseColour")
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
}

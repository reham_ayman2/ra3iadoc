//
//  SendTreatmentVC.swift
//  Ra3ia
//
//  Created by Reham Ayman on 3/24/21.
//

import UIKit

class SendTreatmentVC: UIViewController , UIActionSheetDelegate {
    
    var presenter : SendTreatmentpresenter!
    var isSelected = false
    
    
    
    //MARK: - OUTLETS
    
    @IBOutlet weak var selectTextField: UITextField!
    @IBOutlet weak var ConfirmOutlet: UIButton!
    
    @IBOutlet weak var selectView: UIView!
    @IBOutlet weak var tableview: UITableView!
    
    @IBOutlet weak var searchTextField: UITextField!
    
    
   
    //MARK: - VIEWDIDLOAD

    override func viewDidLoad() {
        super.viewDidLoad()

        ConfirmOutlet.AddTOPCorners(num: 20)
        presenter = SendTreatmentpresenter(view: self)
        SetUpTableview()


    }
    
    
    
    //MARK: - IBACTIONS
    @IBAction func ConfirmButton(_ sender: UIButton) {
    }
    
    
    @IBAction func backButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func selectFromSheet(_ sender: UIButton) {
        
        
        HandelAlertSheet()
       
    }
    
    @IBAction func AddButton(_ sender: UIButton) {
        if isSelected == true {
        sender.backgroundColor = #colorLiteral(red: 0.8196078431, green: 0.8196078431, blue: 0.8392156863, alpha: 1)
            isSelected = false
            
        } else {
            sender.backgroundColor = UIColor(named: "BaseColour")
            isSelected = true
        }
    }
    
    
    
    
    
    
}

//
//  RecipesCell.swift
//  Ra3ia
//
//  Created by Reham Ayman on 3/23/21.
//

import UIKit

class RecipesCell: UITableViewCell , RecipesCellView{
    
    //MARK: - OUTLETS
    
    @IBOutlet weak var requestNum: UILabel!
    @IBOutlet weak var requestDate: UILabel!
    @IBOutlet weak var cost: UILabel!
    @IBOutlet weak var ProdutsNum: UILabel!
    
    
    //MARK: - CYCLE
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    //MARK: - PRESENTER PROTOCOL
  

    func setDateOfRequests(date: String) {
        
    }
    
    func setProductsNum(num: String) {
        
    }
    
    func setCost(cost: String) {
        
    }
    
    func RequestNum(num: String) {
        
    }
    
}

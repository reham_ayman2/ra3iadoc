//
//  ClientCell.swift
//  Ra3ia
//
//  Created by Reham Ayman on 3/23/21.
//

import UIKit
import Cosmos

class ClientCell: UITableViewCell , ClientCellsView{
  
    //MARK: - OUTLETS

    @IBOutlet weak var ClientRate: CosmosView!
    @IBOutlet weak var ClientName: UILabel!
    @IBOutlet weak var ClientImage: UIImageView!
    
    //MARK: - cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.ClientImage.MakeRounded()
        
        
    }
    

    //MARK: - presenter protocols

    func displayClientImage(image: String) {
        self.ClientImage.image = UIImage(named: image)
      }
      
      func setRates(Rate: Int) {
        self.ClientRate.rating = Double(Rate)
      }
      
      func SetClientName(name: String) {
        self.ClientName.text = name
      }
    

   

}

//
//  TypeCell.swift
//  Ra3ia
//
//  Created by Reham Ayman on 3/31/21.
//

import UIKit
import BEMCheckBox

class TypeCell: UICollectionViewCell , RegisterTypeCellView{
  

    @IBOutlet weak var typeLabel: UILabel!
    
    @IBOutlet weak var checkBox: BEMCheckBox!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        checkBox.boxType = .square
       
    }

    
    
    
    func setType(Type: String) {
        self.typeLabel.text = Type
      }
      
}

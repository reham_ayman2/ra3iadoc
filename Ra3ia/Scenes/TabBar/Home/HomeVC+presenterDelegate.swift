//
//  ConversationVc+presenterDelegate.swift
//  Ra3ia
//
//  Created by Reham Ayman on 3/22/21.
//

import UIKit
import ViewAnimator

extension HomeVC : HomeView {
    

    
    func Connected(bool: Bool) {
        if bool == true {

            // connected
            self.tableview.isHidden = false
            self.DissconnectedView.isHidden = true
            featchData()
            timerCounting = true
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(TimerCounter), userInfo: nil, repeats: true)


        } else {

            // dissconnected

            self.tableview.isHidden = true
            self.DissconnectedView.isHidden = false
            self.count = 0
            self.timer.invalidate()
            self.TIMER.text = self.makeTimeString(hours: 0, min: 0, sec: 0)

        }
    }

    func GotoTextChat() {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "ChatVC") as? ChatVC {
            self.navigationController?.pushViewController(vc, animated: true)
        }}
    
    func goToNotifications() {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "notificationsVC") as? notificationsVC {
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
   
  
    
    func featchData() {
        self.tableview.reloadData()
        animateView()
    }
    
    
    func animateView () {
        
        let ANIMATION = AnimationType.zoom(scale: 0.2)
        let fromAnimation = AnimationType.vector(CGVector(dx: 30, dy: 0))
       // let rotateAnimation = AnimationType.rotate(angle: CGFloat.pi/4)
        UIView.animate(views: tableview.visibleCells, animations: [fromAnimation, ANIMATION], duration: 0.8 )

    }
    func makeTimeString (hours: Int , min : Int , sec : Int ) -> String{
        var timeString = ""
        timeString += String(format: "%02d", hours)
        timeString += " : "
        
        timeString += String(format: "%02d", min)
        timeString += " : "
        
        timeString += String(format: "%02d", sec)
       
        return timeString
        
        
    }
    func secondToMinToHours (seconds : Int) -> (Int, Int , Int) {
        
        // sec    min   hours
        return (seconds / 3600 , ((seconds % 3600) / 60) ,   ((seconds % 3600) % 60 )    )
    }
    
}

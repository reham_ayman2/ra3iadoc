//
//  ConversationVC.swift
//  Ra3ia
//
//  Created by Reham Ayman on 3/22/21.
//

import UIKit
import ViewAnimator

class HomeVC: UIViewController {
    //MARK: - vars
    var presenter : HomeVcPresenter!
    var timer : Timer = Timer()
    var count : Int = 0
    var timerCounting : Bool = false
    
    
   
    
    
    
    
    //MARK: - Outlets
    @IBOutlet weak var TIMER: UILabel!
    @IBOutlet weak var switchOutlet: UISwitch!
    @IBOutlet weak var MessageRequests: UILabel!
    @IBOutlet weak var UserImage: UIImageView!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var DissconnectedView: UIView!
    
    
    
    //MARK: - cycle
    
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.DissconnectedView.isHidden = true
        SetUpTableview()
        UserImage.MakeRounded()
        presenter = HomeVcPresenter(view: self)
        
        
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.animateView()
    }
    
    //MARK: - IBActions
    
    @IBAction func NotificationPressed(_ sender: UIButton) {
        
        goToNotifications()
    }
    
    
    @IBAction func swichPressed(_ sender: UISwitch) {
        if sender.isOn {
            // active
          Connected(bool: true)
            
            
        } else {
            // dissconnected
        Connected(bool: false)
            
           
            
            
            
        }
        
    }
    
    
    
    
    
    
    
    
    
    @objc func TimerCounter() {
        count = count + 1
        let time = secondToMinToHours(seconds: count)
        let timeString = makeTimeString(hours: time.0, min: time.1, sec: time.2)
        TIMER.text = timeString
    }
    
 
    


}

//
//  ConversationVc+presenterDelegate.swift
//  Ra3ia
//
//  Created by Reham Ayman on 3/22/21.
//

import UIKit
import ViewAnimator
extension ConversationsVC : ConvView {
    func FeatchData() {
        self.tableview.reloadData()
        self.animateView()
        self.tableview.tableFooterView = UIView()
        
    }

    func GotoTextChat() {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "ChatVC") as? ChatVC {
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    
    func goToNotifications() {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "notificationsVC") as? notificationsVC {
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    func animateView () {
        
        let ANIMATION = AnimationType.zoom(scale: 0.2)
        let fromAnimation = AnimationType.vector(CGVector(dx: 30, dy: 0))
//        let rotateAnimation = AnimationType.rotate(angle: CGFloat.pi/4)
        UIView.animate(views: tableview.visibleCells, animations: [fromAnimation, ANIMATION], duration: 0.8 )

    }
    
    
    
}

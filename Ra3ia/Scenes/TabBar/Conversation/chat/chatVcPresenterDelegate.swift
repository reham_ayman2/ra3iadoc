//
//  chatVcPresenter.swift
//  Ra3ia
//
//  Created by Reham Ayman on 3/27/21.
//

import Foundation
extension ChatVC : chatView {
    func fetchingData() {
        self.tableview.reloadData()
    }
    
    func showSettingView() {
        self.SettingView.isHidden = false
        
    }
    func hideSettingView() {
        self.SettingView.isHidden = true
        
    }
    
    func setUpTable() {
        tableview.delegate = self
        tableview.dataSource = self
    }
    
    func goToSendTreatment () {
        
        if let vc = storyboard?.instantiateViewController(withIdentifier: "SendTreatmentVC") as? SendTreatmentVC {
          self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
    
}


//
//  ChatVC.swift
//  Ra3ia
//
//  Created by Reham Ayman on 3/25/21.
//

import UIKit

class ChatVC: UIViewController {
    
    //MARK: - OUTLETS
    var isSelected = false
 
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var chatTextField: UITextField!
    @IBOutlet weak var SettingView: UIView!
    
    
    //MARK: - VIEWDIDLOAD
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTable()
        hideSettingView()

    }
    
    //MARK: - ACTIONS
    
    @IBAction func SendPressed(_ sender: UIButton) {
      
        
       
    }
    
    @IBAction func MoreActions(_ sender: UIButton) {
        if isSelected {
           
            showSettingView()
            isSelected = false
            
        } else  {
           
            hideSettingView()
            isSelected = true
            
            
        }
    }
    
    
    @IBAction func BACK(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
 
    @IBAction func sendTeratment(_ sender: UIButton) {
        
        goToSendTreatment()
        
    }
    @IBAction func chatEnd(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

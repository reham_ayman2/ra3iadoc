//
//  ConversationVc+Tabelview.swift
//  Ra3ia
//
//  Created by Reham Ayman on 3/22/21.
//

import UIKit

extension ConversationsVC : UITableViewDelegate , UITableViewDataSource {
    
    
    func SetUpTableview () {
        tableview.delegate = self
        tableview.dataSource = self
        tableview.register(UINib(nibName: "ConversationReqCell", bundle: nil), forCellReuseIdentifier: "ConversationReqCell")
        
        
        
    }
    
    
    
    
    
    //MARK: - TABLE VIEW METHODS
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if SegementOutlet.selectedSegmentIndex == 0 {
            
            // return ongoing conversation messages
            // presenter.GetOngoingMessagesCount()
            
            return 5
        }else {
            
            
            // return finished conversation messages
            // presenter.GetFinishedMessagesCount()
          
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableview.dequeueReusableCell(withIdentifier: "ConversationReqCell", for: indexPath) as! ConversationReqCell
        presenter.configure(cell: cell, for: indexPath.row)
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        GotoTextChat()
    }
    
    
}

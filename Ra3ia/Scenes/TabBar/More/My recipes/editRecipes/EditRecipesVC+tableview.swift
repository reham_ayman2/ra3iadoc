//
//  EditRecipesVC+tableview.swift
//  Ra3ia
//
//  Created by Reham Ayman on 3/23/21.
//

import UIKit
extension EditRecVC : UITableViewDelegate , UITableViewDataSource {

func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return presenter.getcellsCount()
}

func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableview.dequeueReusableCell(withIdentifier: "editRecipesCell", for: indexPath) as! editRecipesCell
    
    presenter.configureEDITRecipesCells(cell: cell, for: indexPath.row)
    
    
    return cell
}
func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 90
}
    
    
    
func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
    let delete = UIContextualAction(style: .destructive , title: "Delete") { (action, view, completion) in
        
        self.presenter.remove(for: indexPath.row)
        
        
        
        self.tableview.beginUpdates()
        self.tableview.deleteRows(at: [indexPath], with: .automatic)
        self.tableview.endUpdates()
        
        completion(true
        
        
        )
        
       
        
    }
    delete.image = UIImage(named: "Trash-circle")
    
    
    return UISwipeActionsConfiguration(actions: [delete])
    }
}









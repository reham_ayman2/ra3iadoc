//
//  EditRecVC.swift
//  Ra3ia
//
//  Created by Reham Ayman on 3/23/21.
//

import UIKit

class EditRecVC : UIViewController  {
    var presenter : editRecipesVCpresenter!

    @IBOutlet weak var tableview: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = editRecipesVCpresenter(view: self)
        SetUpTableview()
        
    }
    @IBAction func editButton(_ sender: UIButton) {
    }
    
    @IBAction func BackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func Confirm(_ sender: UIButton) {
        
    }
}

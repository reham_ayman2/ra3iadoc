//
//  MoreVcPresenterDelegate.swift
//  Ra3ia
//
//  Created by Reham Ayman on 3/21/21.
//

import UIKit
import ViewAnimator




extension MoreVC : MoreView {
    
 
 
    

    func goToNotifications() {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "notificationsVC") as? notificationsVC {
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    func fetchingDataSuccess() {
        self.tableview.reloadData()
        presenter.showAnimation()
        
    }
    
 
    func showAnimated() {
        let fromAnimation = AnimationType.vector(CGVector(dx: 40, dy: 0))

                      UIView.animate(views: tableview.visibleCells, animations: [fromAnimation], duration: 0.8 )

    }
    
    
}

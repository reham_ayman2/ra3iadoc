//
//  personalInfo+presenterDelegate.swift
//  Ra3ia
//
//  Created by Reham Ayman on 3/30/21.
//

import UIKit
extension PersonalInfoVC : PersonalInfoView {
    
    
    
    func fetchingDataSuccess() {
        self.typeCollectionView.reloadData()
        self.specCollectionView.reloadData()
        
    }
    
    func setupCollections() {
     
        
        
        typeCollectionView.dataSource = self
        typeCollectionView.delegate = self
        specCollectionView.dataSource = self
        specCollectionView.delegate = self
        typeCollectionView.register(UINib(nibName: "TypeCell", bundle: nil), forCellWithReuseIdentifier: "TypeCell")
        specCollectionView.register(UINib(nibName: "specCell", bundle: nil), forCellWithReuseIdentifier: "specCell")
        
    }
    
    func setUpTabel() {
        addTableview.delegate = self
        addTableview.dataSource = self
        addTableview.isHidden = true
        addTableview.isEditing = true
    }
    
    
    func makeAvatarRounded() {
        uesrImage.MakeRounded()
    }
    
    func addButtonCorners() {
        confirmbtnOutlet.AddTOPCorners(num: 25)
    }
    
}

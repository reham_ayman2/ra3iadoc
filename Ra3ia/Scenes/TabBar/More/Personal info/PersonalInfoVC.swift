//
//  PersonalInfoVC.swift
//  Ra3ia
//
//  Created by Reham Ayman on 3/22/21.
//

import UIKit

class PersonalInfoVC: UIViewController {
    
    
    var ExperianseArray = [String]()
    
    var presenter : PersonalInfoVCpresenter!
    //MARK: - OUTLETS
    
    @IBOutlet weak var addTableview: UITableView!
    @IBOutlet weak var uesrImage: UIImageView!
    @IBOutlet weak var confirmbtnOutlet: UIButton!
    @IBOutlet weak var specCollectionView: UICollectionView!
    @IBOutlet weak var typeCollectionView: UICollectionView!
    
    @IBOutlet weak var newEXpTxtField: EditableTextField!
    
    
    //MARK: - cycle
   
    override func viewDidLoad() {
        super.viewDidLoad()
       
        presenter = PersonalInfoVCpresenter(view: self)
        presenter.viewDiDload()
        
    

     
    }
    //MARK: - IBActions

   
    @IBAction func confirmButton(_ sender: UIButton) {
        
        
    }
    
    
    @IBAction func addNewExp(_ sender: UIButton) {
        self.addTableview.isHidden = false
        if newEXpTxtField.text?.isEmpty == false {
            ExperianseArray.append(newEXpTxtField.text!)
            let indexpatth = IndexPath(row: ExperianseArray.count - 1 , section: 0 )
            addTableview.beginUpdates()
            addTableview.insertRows(at: [indexpatth], with: .top)
            addTableview.endUpdates()
            newEXpTxtField.text = ""
        }
        
    }
    
    
}

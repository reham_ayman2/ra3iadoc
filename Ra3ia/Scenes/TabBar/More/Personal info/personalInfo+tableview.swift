//
//  personalInfo+tableview.swift
//  Ra3ia
//
//  Created by Reham Ayman on 4/1/21.
//

import UIKit
extension PersonalInfoVC : UITableViewDelegate , UITableViewDataSource {
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return ExperianseArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = addTableview.dequeueReusableCell(withIdentifier: "addExperianceCell", for: indexPath) as! addExperianceCell
        cell.experianceLabel.text = ExperianseArray[indexPath.row]
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        ExperianseArray.swapAt(sourceIndexPath.row, destinationIndexPath.row)
    }
  
    
}

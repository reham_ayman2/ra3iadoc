//
//  NotificationVC+tableview.swift
//  Ra3ia
//
//  Created by Reham Ayman on 3/28/21.
//

import UIKit
extension notificationsVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath) as! notificationCell
        
        if indexPath.row % 2 == 0 {
            cell.contentView.backgroundColor = #colorLiteral(red: 0.9336380959, green: 0.9130294323, blue: 0.9501001239, alpha: 1)
            cell.notificationLabel.textColor = UIColor(named: "BaseColour")

        } else if indexPath.row % 2 != 0 {
            cell.contentView.backgroundColor = .white
            cell.notificationLabel.textColor = .darkGray
          }
        
        return cell
        
        
    }
  
    
    
    
    

    
    
}

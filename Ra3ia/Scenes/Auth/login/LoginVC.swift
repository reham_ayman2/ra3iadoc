//
//  LoginVC.swift
//  Ra3ia
//
//  Created by Reham Ayman on 3/21/21.
//

import UIKit

class LoginVC: UIViewController {

    //MARK: - OUTLETS
    

    
    //MARK : - VIEWDIDLOAD
    override func viewDidLoad() {
        super.viewDidLoad()
    
    }
    
    
    
    
    //MARK: - IBACTIONS
    
    @IBAction func Login(_ sender: UIButton) {
        if let vc = storyboard?.instantiateViewController(identifier: "HomeNavigationController") {
            
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func ForgetPassword(_ sender: UIButton) {
       GoToActivationVC()
    }
    
    
    @IBAction func register(_ sender: UIButton) {
        GotoRegister()
    }
    
}


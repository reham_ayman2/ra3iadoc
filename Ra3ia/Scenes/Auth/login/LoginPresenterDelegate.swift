//
//  LoginPresenterDelegate.swift
//  Ra3ia
//
//  Created by Reham Ayman on 3/25/21.
//

import Foundation
extension LoginVC : LoginView {
    
    func GoToActivationVC () {
        if let vc = storyboard?.instantiateViewController(identifier: "ActivationCodeVC") as? ActivationCodeVC {
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    func GotoRegister () {
        if let vc = storyboard?.instantiateViewController(identifier: "RegisterVC") as? RegisterVC {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    
    
    
}

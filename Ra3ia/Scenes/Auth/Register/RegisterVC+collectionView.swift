//
//  RegisterVC+collectionView.swift
//  Ra3ia
//
//  Created by Reham Ayman on 3/24/21.
//

import UIKit
extension RegisterVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.TypeCollectionView {
            return presenter.getTypesCellsCount()
        } else {
            return presenter.getSpecializationCellCount()
        }
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.TypeCollectionView {
            let cell = TypeCollectionView.dequeueReusableCell(withReuseIdentifier: "TypeCell", for: indexPath) as! TypeCell
            presenter.configerTypeCells(cell: cell, for: indexPath.row)
            
            
           return cell
            
        } else {
            
            let celll = specCollectionView.dequeueReusableCell(withReuseIdentifier: "specCell", for: indexPath) as! specCell
            presenter.configpeclizationCells(cell: celll, for: indexPath.row)
            
            return celll
            
            
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.TypeCollectionView {
            
            return CGSize(width: collectionView.frame.width / 3.2 , height: collectionView.frame.height  / 2.5 )
            
            
            
        } else {
            return CGSize(width: collectionView.frame.width / 2.2 , height: collectionView.frame.height - 5  )
        }
    }
    
    
    
}

//
//  RegisterVC.swift
//  Ra3ia
//
//  Created by Reham Ayman on 3/21/21.
//

import UIKit
import ReCaptcha
import WebKit

class RegisterVC: UIViewController {
    
    //MARK: variables
    var presenter : RegisterVCpresenter!
    
    
    let recaptcha = try? ReCaptcha(
       
        apiKey: "6LernIgaAAAAAJc-GIBaigSQet0mmNL3wEdDWnk-",
        baseURL: URL(string: "https://raayarecaptcha.com")!
    )
   
    
   
    //MARK: OUTLETS
    
    @IBOutlet weak var RetypePassword: curvedTextField!
    @IBOutlet weak var passwordText: UILabel!
    @IBOutlet weak var PhoneTextFeild: curvedTextField!
    @IBOutlet weak var EmailTextField: curvedTextField!
    @IBOutlet weak var specCollectionView: UICollectionView!
    @IBOutlet weak var TypeCollectionView: UICollectionView!
    @IBOutlet weak var lastNameText: curvedTextField!
    @IBOutlet weak var BirthDateTextField: curvedTextField!
    @IBOutlet weak var FirstNameText: curvedTextField!
    
    
    
    //MARK: - VIEWDIDLOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
         presenter = RegisterVCpresenter(view: self)
         setupCollections()
         setupReCaptcha()
         

    }
    
    
    
    
    //MARK: - IBACTIONS
    
    @IBAction func RegisterPressed(_ sender: UIButton) {
         GoToHomePage()
        
    }
    
    @IBAction func CheckRobot(_ sender: UIButton) {
        self.recaptchavalidate()
       
    }
    
    
    
    @IBAction func LoginPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
       
    }
    
    @IBAction func SelectDatePressed(_ sender: UIButton) {
        
       PresentSelectDate()
         
    }
    
    
    
}

//
//  RegisterPresenter.swift
//  Ra3ia
//
//  Created by Reham Ayman on 3/24/21.
//

import Foundation

//MARK:- protocols

protocol RegisterView : class {
    
func fetchingDataSuccess()
func GoToHomePage()
func setupCollections ()
func setupReCaptcha()
func PresentSelectDate ()
}

// type collection cells

protocol RegisterTypeCellView {
    func setType ( Type : String)
    
}

//Specialization collection cells


protocol RegisterSpecializationCellView {
    func setSpecialization ( Specialization : String )
}




//MARK:- presenter class

class  RegisterVCpresenter {
    
    //MARK:- 1 variabals
    
    
    private weak var view : RegisterView?
    //collection vars
    // replace with api struct arrays
    
    
    var Specializations = [ "General practitione" , "Nutritionist" ]
    var Types = ["Dogs","Cats","Dogs","Cats","Dogs","Cats"]
    
    
          
      
    
    
    //MARK:- 2 view initionalizer
    
    init(view : RegisterView) {
        self.view = view
    }
    
    
    
    
    
    //MARK: - 3 class functions
  
    func configpeclizationCells(cell: RegisterSpecializationCellView , for index: Int) {
        // incomplete set data to cell
       
        cell.setSpecialization(Specialization: Specializations[index])
      
    }
    
    func configerTypeCells ( cell : RegisterTypeCellView , for index : Int){
        cell.setType(Type: Types[index])
    }
    
  
    
    
    
    
    func getTypesCellsCount () -> Int {
        return Types.count
    }
    func getSpecializationCellCount () -> Int {
        return Specializations.count
    }
    
    

  
    

}

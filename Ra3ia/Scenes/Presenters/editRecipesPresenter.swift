//
//  editRecipesPresenter.swift
//  Ra3ia
//
//  Created by Reham Ayman on 3/23/21.
//

import Foundation
//MARK:- protocols

protocol editRecipesView : class {
    
    func fetchingDataSuccess()
    func SetUpTableview()
  
}

// cells

protocol EditRecipesCellView {
    func setImage ( image : String)
    func setProductName ( name : String)
    func setAmount ( number : Int)
    func setCost ( cost : Int)
    
    

  
   
  
  
  
}





//MARK:- presenter class

class  editRecipesVCpresenter {
    
    //MARK:- 1 variabals
    
    private weak var view : editRecipesView?
    
    
            // replace with api struct arrays
      var products = ["  1 "," 2 "," 3 "," 4 ","  5 "," 6 "]
   
    
    
    
    
    
    
    
    
    //MARK:- 2 view initionalizer
    
    init(view: editRecipesView) {
        self.view = view
    }
    
    
    
    
    
    //MARK: - 3 class functions
  
    func configureEDITRecipesCells(cell: EditRecipesCellView, for index: Int) {
        // incomplete set data to cell
        cell.setProductName(name: products[index])
        
      
     
      
    }
    func remove (for index : Int ){
        products.remove(at: index)
        
    }
    
    
    
    
    func getcellsCount () -> Int {
        return products.count
    }
    

  
    

}

//
//  MoreVCPresenter.swift
//  Ra3ia
//
//  Created by Reham Ayman on 3/21/21.
//

import Foundation


//MARK:- protocols


protocol MoreView: class {
    
    func fetchingDataSuccess()
    func goToNotifications()
    func showAnimated()
        
   
  
   
              //TODO:-  navigate from table view

}


protocol MoreCellsView {
    func displayMainLabel(name: String)
    func displayDetails(name: String)
    func displayImage(image: String)
}





//MARK:- presenter class

class MoreVcPresenter {
    //MARK:- 1 variabals
    
    private weak var view : MoreView?
    // cells
   
    var imagesArray = ["user","language","clients", "wasf" , "rates" , "stat" , "phone","terms" ,"about" ,"shkwa" ,"logout"]
    
    var mainLabelsArray = ["Personal data","language","My clients","My Prescriptions" ,"My review", "statistics","call us" , "Usage Policy" , "About the app" , "Complaints and suggestions","Logout" ]
   
    var DetailsLabelsArray = ["personal information",
                              "Language control",
                              "See clients",
                              "Review medication prescriptions" ,
                              "You can see your overall rating" ,
                              "You can view the statistics from here" ,
                              "Is there a problem ? We will help you solve it",
                              "Any terms or conditions that are updated in the app",
                              "Abstract" ,
                              "Send your complaint or suggestion here"          ,
                              ""

    ]
    
    //MARK:- 2 view initionalizer
    
    init(view: MoreView) {
        self.view = view
    }
    
    
    
    
   //MARK: - 3 class functions
    
    func getcellsCount() -> Int {
        return mainLabelsArray.count
    }
    
    func configure(cell: MoreCellsView, for index: Int) {
        
        cell.displayMainLabel(name: self.mainLabelsArray[index])
        cell.displayDetails(name: self.DetailsLabelsArray[index])
        cell.displayImage(image: self.imagesArray[index])
        
        
        
       
    }
    
    
    func gotoNotification () {
        view?.goToNotifications()
    }
    
    func showAnimation () {
        view?.showAnimated()
    }
    
 
  
    
}

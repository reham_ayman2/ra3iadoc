//
//  SendTreatmentPresenter.swift
//  Ra3ia
//
//  Created by Reham Ayman on 3/24/21.
//
import Foundation
//MARK:- protocols

protocol SendTreatmentView : class {
    
    func fetchingDataSuccess()
    func SetUpTableview()
  
}

// cells

protocol TreatmentCellView {
   
    func setTreatmentName ( name : String)
    func setCost ( cost : Int)
    
 
}





//MARK:- presenter class

class  SendTreatmentpresenter {
    
    //MARK:- 1 variabals
    
    private weak var view : SendTreatmentView?
    
    
            // replace with api struct arrays
      var treatmentsArray = ["  treatment 1 "," treatment 2 ","treatment 3 ","treatment 4 "," treatment 5 ","treatment 6 "]
   
    
    
    
    
    
    
    
    
    //MARK:- 2 view initionalizer
    
    init(view: SendTreatmentView) {
        self.view = view
    }
    
    
    
    
    
    //MARK: - 3 class functions
  
    func configureTreatmentCells(cell: TreatmentCellView, for index: Int) {
        // incomplete set data to cell
        cell.setTreatmentName(name: treatmentsArray[index])
        
      
     
      
    }
    
  
    
    
    
    
    func getcellsCount () -> Int {
        return treatmentsArray.count
    }
    

  
    

}

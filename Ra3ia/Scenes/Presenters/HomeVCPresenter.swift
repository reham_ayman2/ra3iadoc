//
//  ConversationVCPresenter.swift
//  Ra3ia
//
//  Created by Reham Ayman on 3/22/21.
//

import Foundation


//MARK: - required Protocols ( cells + view )

protocol HomeView : class {
    func featchData ()
    func Connected ( bool : Bool)
    func GotoTextChat()
    func goToNotifications ()
    
    func makeTimeString (hours: Int , min : Int , sec : Int ) -> String
    func secondToMinToHours (seconds : Int) -> (Int, Int , Int)
    
    
    
           
       
    
    // func updateLoginTime()
    
}
protocol ConvCellView {
    func displayImage (image : String)
    func displayMessageContent ( message : String)
    func setRates ( Rate : Int)
    func SetSenderName ( name : String)
    func SetSendingTime ( time : String)
    
}




//MARK: - presenter class
class HomeVcPresenter {
    
    
    //MARK:- 1 variabals
    
    private weak var view : HomeView?
    
        // replace with api arrays
    var Senders = ["user name 1 ","user name 2 ","user name 3 ","user name 4 ","user name 5 ","user name 6 "]
    
    
    var AvatarsArray = [String]()
    var rates = [Int]()
    var Messages = [String]()
    // var time = []()
    
    
    
    //MARK:- 2 view initionalizer
    
    init(view: HomeView) {
        self.view = view
    }
    
   

    //MARK: - 3 class functions
     
     func getcellsCount() -> Int {
        return Senders.count
     }
     
     func configure(cell: ConvCellView, for index: Int) {
        cell.SetSenderName(name: self.Senders[index])
      
     }
    
   
    
    func dissconnectedUser () {
        
    }
 
    
    
}


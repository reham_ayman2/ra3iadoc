//
//  personalInfoPresenter.swift
//  Ra3ia
//
//  Created by Reham Ayman on 3/30/21.
//

import Foundation

//MARK:- protocols

protocol PersonalInfoView : class {
    
func fetchingDataSuccess()
func setupCollections ()
func setUpTabel()
func makeAvatarRounded()
func addButtonCorners()
    

    
}

// note : collections protocol in register presenter






//MARK:- presenter class

class PersonalInfoVCpresenter {
    
    //MARK:- 1 variabals
    
    
    private weak var view : PersonalInfoView?
    //collection vars
    // replace with api struct arrays
    
    
   private var Spec = [ "General" , "Nutritionist" ]
   private var Types = ["Dogs","Cats","Dogs","Cats","Dogs","Cats"]
   private var ExperianseArray = [String]()
    
    
          
      
    
    
    //MARK:- 2 view initionalizer
    
    init(view : PersonalInfoView) {
        // personalinfo view controller
        self.view = view
    }
    
    
    
    
    
    //MARK: - 3 class functions
  
    func configpeclizationCells(cell: RegisterSpecializationCellView , for index: Int) {
        // incomplete set data to cell
       
        cell.setSpecialization(Specialization: Spec[index])
      
    }
    func viewDiDload () {
        view?.setupCollections()
        view?.setUpTabel()
        view?.makeAvatarRounded()
        view?.addButtonCorners()
    }
    
    func configerTypeCells ( cell : RegisterTypeCellView , for index : Int){
        cell.setType(Type: Types[index])
    }
    
    func getTypesCellsCount () -> Int {
        return Types.count
    }
    
    
    func getSpecializationCellCount () -> Int {
        return Spec.count
    }
    
    
//    func addElements (element : String) {
//        
//        
//    }
    

  
    

}

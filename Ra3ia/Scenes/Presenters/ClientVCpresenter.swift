//
//  ClientVCpresenter.swift
//  Ra3ia
//
//  Created by Reham Ayman on 3/23/21.
//

import Foundation
//MARK:- protocols

protocol MYclientsView: class {
    
    func fetchingDataSuccess()
    func SetUpTableview()
  
}

// cells

protocol ClientCellsView {
    func displayClientImage (image : String)
    func setRates ( Rate : Int)
    func SetClientName ( name : String)
  
}





//MARK:- presenter class

class clientVcpresenter {
    
    //MARK:- 1 variabals
    
    private weak var view : MYclientsView?
    
    
            // replace with api arrays
      var Newclients = [" name 1 "," name 2 ","name 3 ","user name 4 "," name 5 ","name 6 "]
    var currentclients = [" name A "," name B ","name C "]
    var ClientImage = [String]()
    var Rate = [Int]()
    
    
    
    
    
    
    
    
    //MARK:- 2 view initionalizer
    
    init(view: MYclientsView) {
        self.view = view
    }
    
    
    
    
    
    //MARK: - 3 class functions
   
           

    
    func configureCurrentCells(cell: ClientCellsView, for index: Int) {
        cell.SetClientName(name: self.currentclients[index])
       // cell.displayClientImage(image: self.ClientImage[index])
       // cell.setRates(Rate: self.Rate[index])
     
    }
    
    func ConfigureNewCells ( cell : ClientCellsView , for index : Int) {
        cell.SetClientName(name: self.Newclients[index])
        //cell.displayClientImage(image: self.ClientImage[index])
       // cell.setRates(Rate: self.Rate[index])
        
    }
    
    
    
    
    func getNewClientsCount () -> Int {
        return Newclients.count
    }
    

    func getCurrentClientsCount () -> Int {
        return currentclients.count
    }
    
    func ViewDidload () {
        view?.SetUpTableview()
    }
    

}

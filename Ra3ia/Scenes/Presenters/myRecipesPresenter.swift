//
//  myRecipesPresenter.swift
//  Ra3ia
//
//  Created by Reham Ayman on 3/23/21.
//

import Foundation
//MARK:- protocols

protocol myRecipesView : class {
    
    func fetchingDataSuccess()
    func SetUpTableview()
  
}

// cells

protocol RecipesCellView {
  
    func setDateOfRequests ( date : String)
    func setProductsNum ( num : String)
    func setCost (cost : String)
    func RequestNum ( num : String)
  
  
  
}





//MARK:- presenter class

class  myRecipesVCpresenter {
    
    //MARK:- 1 variabals
    
    private weak var view : myRecipesView?
    
    
            // replace with api struct arrays
      var requests = ["  1 "," 2 "," 3 "," 4 ","  5 "," 6 "]
   
    
    
    
    
    
    
    
    
    //MARK:- 2 view initionalizer
    
    init(view: myRecipesView) {
        self.view = view
    }
    
    
    
    
    
    //MARK: - 3 class functions
  
    func configureRecipesCells(cell: RecipesCellView, for index: Int) {
        // incomplete set data to cell
        cell.RequestNum(num: self.requests[index])
        
      
     
      
    }
    
  
    
    
    
    
    func getcellsCount () -> Int {
        return requests.count
    }
    

  
    

}

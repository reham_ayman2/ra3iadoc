//
//  LoginPresenter.swift
//  Ra3ia
//
//  Created by Reham Ayman on 3/25/21.
//

import Foundation
protocol LoginView : class {
    func GoToActivationVC ()
    func GotoRegister ()

}
class  LoginVCpresenter {
    
    //MARK:- 1 variabals
    
    
    private weak var view : LoginView?
 
    
    
    //MARK:- 2 view initionalizer
    
    init(view : LoginView) {
        self.view = view
    }
}

//
//  staticsVCpresenter.swift
//  Ra3ia
//
//  Created by Reham Ayman on 3/23/21.
//

import Foundation
//MARK:- protocols

protocol staticksView : class {
    
    func fetchingDataSuccess()
  
  func setup ()
}

// tabelcells

protocol StaticsTabelCellView {
    func setImage ( image : String)
    func SenderName ( name : String)
    func setdetails ( details : String)
   
     
}

//collection cells


protocol StaticsCollectionCellView {
    func setStaticImage ( image : String)
    func setMainNum ( num : String)
    func setDetails ( details : String)
}




//MARK:- presenter class

class  staticsVCpresenter {
    
    //MARK:- 1 variabals
    
    
    private weak var view : staticksView?
    //collection vars
    var Staticsimages = ["coin" , "treet" , "chats" ]
    var details = ["profits percentage","Acceptable treatment","messages"]
    
    
            // replace with api struct arrays
      
    var mainNum = ["100", "200" , "300"]
   
    // tabel vars    -> (todo)
    
    
    
    
    
    
    
    
    //MARK:- 2 view initionalizer
    
    init(view : staticksView) {
        self.view = view
    }
    
    
    
    
    
    //MARK: - 3 class functions
  
    func configurCollectionCells(cell: StaticsCollectionCellView, for index: Int) {
        // incomplete set data to cell
        cell.setMainNum(num: mainNum[index])
        cell.setStaticImage(image: Staticsimages[index])
        cell.setDetails(details: details[index])
        
      
     
      
    }
    
    func configerTabelCells ( cell : StaticsTabelCellView , for index : Int){
        
    }
    
  
    
    
    
    
    func getCollectioncellsCount () -> Int {
        return Staticsimages.count
    }
    
    

  
    

}

//
//  AddClientRateVC.swift
//  Ra3ia
//
//  Created by Reham Ayman on 3/23/21.
//

import UIKit

class AddClientRateVC: UIViewController {
    //MARK: - OUTLETS
    
    @IBOutlet weak var ConfirmOutlet: UIButton!
    
    
    
    //MARK: - VIEWDIDLOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ConfirmOutlet.AddTOPCorners(num: 10.0)

       
    }
    
    //MARK: - IBACTIONS

    @IBAction func ConfirmPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    

}

//
//  ServerPaths.swift
//  SeaTrips
//
//  Created by Sara Mady on 10/27/20.
//  Copyright © 2020 Sara Ashraf. All rights reserved.
//

import Foundation

enum ServerPaths: String {
    
    //MARK:- Authntcation APIS:-
    ///31.0388288/31.3884665
    case intros
    case login
    case register
    case verifyPhone = "verify-phone"
    case forgotPassword = "forgot-password"
    case resetPassword = "reset-password"
    case about
    case agreaments
    case faqs
    case userHome = "user-home"
    case contactUs = "contact-us"
    case singleType = "single-type"
    case singleProvider = "single-provider"
    case addReview = "add-review"
    case singleDestination = "single-destination"
    case singleTrip = "single-trip"
    case addReservtion = "add-reservation"
    case singleReservation = "single-reservation"
    case reservations = "reservations"
    case userCancelReservation = "user-cancel-reservation"
    case logout = "logout"
    case userReservations = "user-reservations"
    case archiveReservation = "archive-reservation"
    case confirmReservation = "confirm-reservation"
    case updateProfile = "update-profile"
    case profile = "profile"
    case unarchiveReservation = "unarchive-reservation"
    
    //MARK:- Provider APIS:-
    
    case providerHome = "provider-home"
    case providerReservations = "provider-reservations"
    case notifications = "notifications"
    case providerTrips = "provider-trips"
    case providerSingleReservation = "provider-single-reservation"
    case providerCancelReservation = "provider-cancel-reservation"
    case addTrip = "add-trip"
    case tripTypes = "trip-types"
    case tripDestinations = "trip-destinations"
    case deleteNotifications = "delete-notifications"
    case finishReservation = "finish-reservation"
    case updatePassword = "update-password"
    case toggleNotifications = "toggle-notifications"
    case editReservation = "edit-reservation"
    case commissions = "commissions"
    case enquiryCopoun = "enquiry-copoun"
    case wallet = "wallet"
    case deleteTrip = "delete-trip"
    case social = "social"
    case editTrip = "edit-trip"
    
}

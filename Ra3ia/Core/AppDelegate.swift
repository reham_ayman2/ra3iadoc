//
//  AppDelegate.swift
//  Ra3ia
//
//  Created by Sara Mady on 21/03/2021.
//

import UIKit
import IQKeyboardManagerSwift


@main
class AppDelegate: UIResponder, UIApplicationDelegate {

   
    var window: UIWindow?

    
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared.enable = true
    
        self.setUpSegment()
        
        return true
    }

    
    
    
    
    
    

}
extension AppDelegate {
    
    
    
    
    func setUpSegment() {
        
        
        if let font = UIFont(name: "Fairuz", size: 16 )
        {
           
            UISegmentedControl.appearance().setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)

        }
        UISegmentedControl.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor:UIColor.white], for: .selected)
        
        
    }
    
    
    
}


